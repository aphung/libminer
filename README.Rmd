---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library set up. It is a toy package from a workshop and not meant for serious use

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("aphung/libminer")

```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(libminer)
lib_summary()

# Or to calculate sizes:
lib_summary(sizes = TRUE)
```


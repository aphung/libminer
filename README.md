
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library set up.
It is a toy package from a workshop and not meant for serious use

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("aphung/libminer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)
lib_summary()
#>                                                                 Library
#> 1                                    C:/Program Files/R/R-4.3.0/library
#> 2                        C:/Users/phung/AppData/Local/R/win-library/4.3
#> 3 C:/Users/phung/AppData/Local/Temp/RtmpSm9ppv/temp_libpath6aa0388544ba
#>   n_packages
#> 1         30
#> 2        435
#> 3          1

# Or to calculate sizes:
lib_summary(sizes = TRUE)
#>                                                                 Library
#> 1                                    C:/Program Files/R/R-4.3.0/library
#> 2                        C:/Users/phung/AppData/Local/R/win-library/4.3
#> 3 C:/Users/phung/AppData/Local/Temp/RtmpSm9ppv/temp_libpath6aa0388544ba
#>   n_packages   lib_size
#> 1         30   68685145
#> 2        435 1022556271
#> 3          1      12540
```
